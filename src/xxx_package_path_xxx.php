<?php

namespace Techendeavors\xxx_package_path_xxx;

use Illuminate\Support\Collection;
use Techendeavors\xxx_package_path_xxx\Traits\Example;

class xxx_package_path_xxx
{
    use Example;

    private const ENABLED    = true;
    private const KEY        = "value";

    protected $config;
    protected $enabled;
    protected $key;

    /**
     * Classes which have a constructor method call this method on each newly-created object, 
     * so it is suitable for any initialization that the object may need before it is used.
     */
    public function __construct()
    {
        $this->config   = config('xxx_package_name_xxx');
        $this->enabled  = $this->config['enabled']         ?: self::ENABLED;
        $this->key      = $this->config['key']             ?: self::KEY;
    }

    /**
     * The __invoke() method is called when a script tries to call an object as a function.
     * https://secure.php.net/manual/en/language.oop5.magic.php#object.invoke
     */
    public function __invoke($variable)
    {
        var_dump($variable); // example
    }

    /**
     * The __toString() method allows a class to decide how it will react when it is treated like a string. For 
     * example, what echo $obj; will print. This method must return a string, as otherwise a fatal E_RECOVERABLE_ERROR 
     * level error is emitted.
     * https://secure.php.net/manual/en/language.oop5.magic.php#object.tostring
     */
    public function __toString()
    {
        return (string) $this->key;
    }

    /**
     * This method is called by var_dump() when dumping an object to get the properties that should be shown. 
     * If the method isn't defined on an object, then all public, protected and private properties will be shown.
     */
    public function __debugInfo()
    {
        $debugInfo = [
            'enabled'   => $this->enabled,
            'key'       => $this->key
        ];

        if ($this->config['debug']) {
            return $debugInfo;
        }
        
        return null;
    }

    /**
     * __call() is triggered when invoking inaccessible methods in an object context.
     * $obj = new MethodTest;
     * $obj->runTest('in object context');
     *
     * Returns: Calling object method 'runTest' in object context
     */
    public function __call($name, $arguments)
    {
        // Note: value of $name is case sensitive.
        echo "Calling object method '$name' "
             . implode(', ', $arguments). "\n";
    }

    /**
     * __call() is triggered when invoking inaccessible methods in a static context.
     * MethodTest::runTest('in static context');
     *
     * Returns: Calling static method 'runTest' in static context
     */
    public static function __callStatic($name, $arguments)
    {
        // Note: value of $name is case sensitive.
        echo "Calling static method '$name' "
             . implode(', ', $arguments). "\n";
    }
}
