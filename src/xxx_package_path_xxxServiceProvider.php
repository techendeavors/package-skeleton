<?php

namespace Techendeavors\xxx_package_path_xxx;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Container\Container;

class xxx_package_path_xxxServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $source = realpath($raw = __DIR__.'/../config/xxx_package_name_xxx.php') ?: $raw;

        $this->publishes([$source => config_path('xxx_package_name_xxx.php')], 'config');
    }

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
        $source = realpath($raw = __DIR__.'/../config/xxx_package_name_xxx.php') ?: $raw;

        $this->app->alias('xxx_package_name_xxx', xxx_package_path_xxx::class);

        $this->mergeConfigFrom($source, 'xxx_package_name_xxx');

        $this->app->singleton('xxx_package_name_xxx', function (Container $app) {
            return new xxx_package_path_xxx();
        });        
    }
}
