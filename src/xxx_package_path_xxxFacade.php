<?php

namespace Techendeavors\xxx_package_path_xxx\Facades;

use Illuminate\Support\Facades\Facade;

class xxx_package_path_xxxFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'xxx_package_name_xxx';
    }
}
