# xxx_package_path_xxx

## Very short description of the package

[![Latest Version on Packagist](https://img.shields.io/packagist/v/techendeavors/xxx_package_name_xxx.svg?style=flat-square)](https://packagist.org/packages/techendeavors/xxx_package_name_xxx)
[![Total Downloads](https://img.shields.io/packagist/dt/techendeavors/xxx_package_name_xxx.svg?style=flat-square)](https://packagist.org/packages/techendeavors/xxx_package_name_xxx)

**Note:** Replace ```xxx_package_name_xxx``` ```xxx_package_path_xxx``` ```xxx_package_description_xxx``` with their correct values throughout this project folder, then delete this line. It's also recommended to rename the files in the `src` folder and prepend the file names with the `xxx_package_path_xxx`

xxx_package_description_xxx

### To Do

- [ ] Write Tests

### Development Process

Before the package is registered with [packagist.org](https://packagist.org), you can work with it locally. First, add the following to your `.bashrc`, `.bash_profile`, or `.zshrc`

```bash
composer-link() {  
    composer config repositories.$1 path "../$1"
    composer require techendeavors/$1 @dev
}
```

Then, in a Laravel installation folder, run `composer-link xxx_package_name_xxx` and it'll add the package information to the repositories section of the composer.json file and run composer update.

You can [find more information here](http://calebporzio.com/bash-alias-composer-link-use-local-folders-as-composer-dependancies/).

### Installation

You can install the package via composer:

```bash
composer require techendeavors/xxx_package_name_xxx
```

### Usage

``` php
$skeleton = new Techendeavors\xxx_package_path_xxx();
echo $skeleton->echoPhrase('Hello, Techendeavors!');
```

### Testing

``` bash
composer test
```

### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

### Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

### Security

If you discover any security related issues, please use the [issue tracker](https://gitlab.com/techendeavors/xxx_package_name_xxx)

### Credits

- [Techendeavors](https://gitlab.com/techendeavors)
- [Contributors and Credits](CONTRIBUTORS.md)



### License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
